import { oso, pajaro, puente, sapo } from "../../assets/";

import style from './styles/certificate.module.css';

const Certificate = () => {
  return (
    <div className={style.gallery}>
      <div className={style.galleryItem}>
        <img src={oso} alt="Bear" />
        <button className={style.seeMore}>
          See more <i className="uil uil-angle-right-b"></i>
        </button>
      </div>
      <div className={style.galleryItem}>
        <img src={pajaro} alt="Bird" />
        <button className={style.seeMore}>
          See more <i className="uil uil-angle-right-b"></i>
        </button>
      </div>
      <div className={style.galleryItem}>
        <img src={puente} alt="Bridge" />
        <button className={style.seeMore}>
          See more <i className="uil uil-angle-right-b"></i>
        </button>
      </div>
      <div className={style.galleryItem}>
        <img src={sapo} alt="Frog" />
        <button className={style.seeMore}>
          See more <i className="uil uil-angle-right-b"></i>
        </button>
      </div>
    </div>
  );
};

export default Certificate;
