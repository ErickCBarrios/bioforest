import "./styles/side_bar.css";

import { buy, category, map, payment, product, profile, user } from "../../assets/index";

// import icono1 from "../../assets/icon/logo.svg";

const Side_Bar = () => {
  return (
    <div id="nav-bar">
      <input id="nav-toggle" type="checkbox" />
      <div id="nav-header">
        <a id="nav-title" href="#">
          Dashboard
        </a>
        <label htmlFor="nav-toggle">
          <span id="nav-toggle-burger"></span>
        </label>
        <hr />
      </div>
      <div id="nav-content">
      <div className="nav-button">
          <span>Account settings</span>
        </div>
        <hr />
        <div className="nav-button">
          <img className="icon" src={user} />
          <span>User</span>
        </div>
        <div className="nav-button">
          <img className="icon" src={profile} />
          <span>Profile</span>
        </div>
        <div className="nav-button">
        <img className="icon" src={payment} />
          <span>Payment</span>
        </div>
        <div className="nav-button">
        <img className="icon" src={category} />
          <span>Category</span>
        </div>
        <div className="nav-button">
        <img className="icon" src={product} />
          <span>Product</span>
        </div>
        <div className="nav-button">
        <img className="icon" src={buy} />
          <span>Buy</span>
        </div>
        <div className="nav-button">
        <img className="icon" src={map} />
          <span>Map</span>
        </div>


    
      </div>
      <div id="nav-footer">
        <div id="nav-footer-heading">
          <div id="nav-footer-avatar">
          <i className="uil uil-signout"></i>
          </div>
          <div id="nav-footer-titlebox">
            <a id="nav-footer-title" href="#" >log out</a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Side_Bar;
