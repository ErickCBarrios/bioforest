import { expand, flag, menu, moon, usernav } from "../../assets/";

import style from "./styles/navbar.module.css";

const Navbar = () => {
  return (
    <>
      <div className={style.container}>
        <div className={style.iconMenu}>
          <img src={menu} alt="" />
        </div>
        <div className={style.input}>
              <input type="text" placeholder="Search" />
        </div>
        <div className={style.container2}>
        <div className={style.containerIcons}>
            <img src={flag} alt="" />
            <img className={style.iconMoon} src={moon} alt="" />
            <img src={expand} alt="" />
        </div>
        <div>
              <button><img className={style.buttonnav} src={usernav} alt="" />Randy salas</button>
        </div>
        </div>



      </div>

      
    </>
  );
};
export default Navbar;
