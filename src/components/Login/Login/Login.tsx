import {lock, logo_login, register, user_login, user_profile} from "../../../assets/"

import style from "./styles/login.module.css"
import { useState } from "react"

const Login = () => {
  const [active, setActive] = useState("login");

  return (
    <section> 
    <div className={style.container_login}>
        <div className={style.card_login_general}>
            <div className={style.tabs}>
                <button onClick={
                  () => {
                    setActive("login");
                  }
                } id="loginTab" className={active === "login" ? style.active : style.desactivelog}><i className={`uil uil-signout ${style.icon}`}></i>Ingresar</button>
                <button onClick={
                  () => {
                    setActive("registro");
                  }
                } id="registerTab" className={active === "registro" ? style.activereg : style.desactivereg}><img src={register} className={style.registericon} />Registro</button>
            </div>
        {active === "login" ? (
                  <div className={`${style.tabs_content_login} ${style.active}`}  id="loginContent">
                  <div className={style.login} >
                       <img className={style.img_login} src={user_profile} width="50px" height="50px" alt=""/>
                       <div className={style.containerInput}>
                           <img  className={style.imgInputUser} src={user_login} alt="" />
                           <input type="text" placeholder="Nombre de usuario o correo"/>
                       </div>
                       <div className={style.containerReg}>
                           <img  className={style.imgInputReg} src={lock} alt="" />
                           <input type="password" placeholder="Contraseña"/>
                       </div>
                        
                        <div className={style.olvidar_contraseña}>
                            <div className={style.containerRec}>
                                <input className={style.inputRec} type="checkbox" name="recordar" id="recordar"/>
                                <label htmlFor="recordar" className={style.labelRec}>Recuerdame</label>
                                <div className={style.olvCont}><a  href="">Olvide la contraseña</a></div>
                            </div>
                        </div>
                        <div className={style.containerLog}>
                              <button>Ingresar</button>
                        </div>
                            
                    </div>
                </div>
        ) : active === "registro" ? (
        <div className={`${style.tabs_content_register} ${style.active}`} id="registerContent">
        <div className={style.register} >
            <img className={style.img_login} src={user_profile} width="50px" height="50px" alt=""/>
            <input type="text" placeholder="Nombre de usuario o correo"/>
            <input type="password" placeholder="Contraseña"/>
            <input type="email" placeholder="Correo"/>
            <div className={style.containerLog}>
                <button>Registarse</button>
            </div>
        </div>
    </div>
): (<p>Error inesperado</p>)}
       </div>
        <img width="300px" src={logo_login} alt=""/>
      
    </div>

</section>
  )
}

export default Login