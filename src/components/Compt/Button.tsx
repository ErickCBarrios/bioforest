import './styles/button.css'

const Button = () => {
  return (
    <div className='containerbt'>
      <h1>Buttom</h1>
      <div className="button-container">
        <button className="btn btn-activated">
          <span ><i className={`uil uil-estate icon`}></i></span> Activado
         </button>
        <button className="btn btn-active">
          Activo <span className="icon"><i className="uil uil-angle-right-b arrow"></i></span>
        </button>
      </div>
      <div className="button-container">
        <button className="btn btn-deactivated">
          <span ><i className={`uil uil-estate icon`}></i></span> Desactivado
        </button>
        <button className="btn btn-disabled">
          Desactivado <span className="icon"><i className="uil uil-angle-right-b arrow"></i></span>
        </button>
      </div>
      <div className="button-container">
        <button className="btn btn-hover">
          <span ><i className={`uil uil-estate icon`}></i></span> Hover
        </button>
        <button className="btn btn-hover">
          Hover <span className="icon"><i className="uil uil-angle-right-b arrow"></i></span>
        </button> 
      </div>
    </div>
  );
};
export default Button