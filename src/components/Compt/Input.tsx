import './styles/input.css'

const Input = () => {
  return (
    <div className='containerip'>
      <h1>Input</h1>
    <div className="input-container">
        <label htmlFor="input-texto">Texto</label>
        <input type="text" id="input-texto" className="input-texto"/>
    </div>
    <div className="input-container">
        <label htmlFor="input-correcto">Correcto</label>
        <input type="text" id="input-correcto" className="input-correcto"/>
    </div>
    <div className="input-container">
        <label htmlFor="input-error">Error</label>
        <input type="text" id="input-error" className="input-error"/>
    </div>


    </div>
  )
}
export default Input