import style from './styles/calculator.module.css';

const Calculator = () => {
  return (
    <div className={style.calculatorContainer}>
      <h1>CO2 Calculator</h1>
      <form className={style.form}>
        <div className={style.containerinputPrinc}>
  
          <label htmlFor="result">Result</label>
          <input type="text" id="result" name="result" />
        
        </div>

        <div className={style.container}>
        <div className={style.formGroup}>
          <label htmlFor="electricity">Billed monthly electricity consumption</label>
          <input type="text" id="electricity" name="electricity" />
        </div>
        <div className={style.formGroup}>
          <label htmlFor="fossilFuel">Fossil fuel consumption</label>
          <input type="text" id="fossilFuel" name="fossilFuel" />
        </div>
        </div>
        <div className={style.container}>
        <div className={style.formGroup}>
          <label htmlFor="fuelGas">Fuel consumption gas</label>
          <input type="text" id="fuelGas" name="fuelGas" />
        </div>
        <div className={style.formGroup}>
          <label htmlFor="trees">Cantidad de arboles apadrinados</label>
          <input type="text" id="trees" name="trees" />
        </div>
        </div>
        <button type="submit" className={style.submitButton}>Show results</button>
      </form>
    </div>
  );
};

export default Calculator;
