import {Navbar, Side_Bar} from '..'

import { ReactNode } from "react";
import style from "./styles/layout.module.css"

interface Props {
  children?: ReactNode;
}

const Layout = ({children}:Props) => {
  return (
    <div >
        <Side_Bar/>
        <Navbar/>
        <div className={style.body}>
          <section>{children}</section>
        </div>
    </div>


  )
}
export default Layout