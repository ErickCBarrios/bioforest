import buy from './icon/buy.svg'
import category from './icon/category.svg'
import expand from "./icon/expand.svg"
import flag from "./icon/flag.svg"
import fondo_login from "./img/fondo_login.png"
import home from './icon/home.svg'
import lock from "./icon/lock.svg"
import logo_login from "./img/logo_bio.png"
import map from './icon/map.svg'
import menu from "./icon/menu.svg"
import moon from "./icon/moon.svg"
import oso from "./img/oso.png"
import pajaro from "./img/pajaro.png"
import payment from './icon/payments.svg'
import product from './icon/product.svg'
import profile from './icon/profile.svg'
import puente from "./img/puente.png"
import register from "./icon/register.svg"
import sapo from "./img/sapo.png"
import user from './icon/user.svg'
import user_login from "./icon/user_login.svg"
import user_profile from "./img/user_profile.png"
import usernav from "./icon/userbtn.svg"

export {
    home,
    user,
    profile,
    payment,
    category,
    product,
    buy,
    map,
    fondo_login,
    logo_login,
    user_profile,
    register,
    user_login,
    lock,
    expand,
    menu,
    moon,
    flag,
    pajaro,
    sapo,
    puente,
    oso,
    usernav,
}
