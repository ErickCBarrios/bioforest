import './assets/styles/global.css'

import {Calculator, Certificate, Layout, Login} from './components/';
import { Route, Routes } from 'react-router-dom'

function App() {

  return (
    <>
      <Routes>
        <Route path='/login' element={<Login/>}/>
        <Route path='/' element={<Layout/>}/>
        <Route path='/certificate' element={<Layout children={<Certificate/>}/>}/>
        <Route path='/calculator' element={<Layout children={<Calculator/>}/>}/>
      </Routes>
    </>
  )
}

export default App
